import { useState, useEffect, useContext } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView() {

    const {user} = useContext(UserContext);

    // "useParams" hooks allows us to retrieve the productId passed via the URL.
    const { productId } = useParams();

    const navigate  = useNavigate();

    // Create state hooks to capture the information of a specific product and display it in our application.
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState('');
    const [stocks, setStocks] = useState(1)

    // Create a state to determine whether the submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if(quantity <= 0){
            setIsActive(false)
        } else {
            setIsActive(true)
        }
    }, [quantity]);


    useEffect(() => {
        console.log(productId);

        fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
        .then(res => res.json())
        .then(data => {

            console.log(data);

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stocks);

        });
    }, [productId]);


    const order = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/users/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                products: [
                    {
                        productId: productId,
                        productName: name,
                        quantity: quantity
                    }
                ],
                totalAmount: quantity * price
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            // if data is true - meaning order is successful
            if(data) {
                if (quantity === 0 || quantity <= 0) {
                    Swal.fire({
                        title: "Something Went Wrong!",
                        icon: "error",
                        text: "Please input valid quantity."
                    });
                    
                } else {
                    Swal.fire({
                        title: "Order placed. Thank you!",
                        icon: "success",
                        text: "Want to purchase again?"
                    });
                    navigate("/products");
                }

            } else {
                Swal.fire({
                    title: "Something Went Wrong!",
                    icon: "error",
                    text: "Please try again."
                });
            }

        });
    };

    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title className="fs-3">{name}</Card.Title>
                            <Card.Subtitle className="fs-4">Description:</Card.Subtitle>
                            <Card.Text className="fs-5">{description}</Card.Text>
                            <Card.Subtitle className="fs-4">Price:</Card.Subtitle>
                            <Card.Text className="fs-5" style={{ color: "blue" }}>Php {price}.00</Card.Text>
                            <div className="align-items-center justify-content-center w-auto text-center">
                                <Form className="px-5 mb-3">
                                    <Form.Control
                                        type="number"
                                        min={0}
                                        placeholder="Quantity"
                                        value={quantity}
                                        onChange={e => setQuantity(e.target.value)}
                                        required
                                    />
                                </Form>
                            </div>
                            
                            {
                                (user.id !== null)
                                ?
                                    (isActive)
                                    ?
                                        <>
                                            <div className="text-center mt-3">
                                                <Button as={Link} to={"/products"} className="mx-3" variant="secondary" size="md">Go Back</Button>
                                                <Button variant="success" size="md" onClick={() => order(productId)}>Buy Now</Button>
                                            </div>
                                        </>
                                    :
                                        <>
                                            <div className="text-center mt-3">
                                                <Button as={Link} to={"/products"} className="mx-3" variant="secondary" size="md">Go Back</Button>
                                                <Button variant="success" size="md" onClick={() => order(productId)} disabled>Buy Now</Button>
                                            </div>
                                        </>
                                    
                                :  
                                    <Button as={Link} to="/login" variant="success" size="lg">Login to Buy</Button>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};