import { useEffect, useState } from "react";

import { Table } from "react-bootstrap";


export default function Orders() {

    const [allOrders, setAllOrders] = useState([]);

    const fetchData = () => {
        // get all the user orders from the database
        fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllOrders(data.orders.map(order => {
                return (
                    <tr className="text-center order-table" key={order._id}>  
                        <td>{order._id}</td>
                        <td>{order.products[0].productName}</td>
                        <td>{order.products[0].quantity}</td>
                        <td>{order.totalAmount}</td>  
                    </tr>    
                );
            }));
        });
};

    // to fetch all courses in the first render of the page.
    useEffect(() => {
        fetchData();
    }, []);



    return (
        <>
            <div className="text-center">
                <div>
                    <h1 className="my-4">Order History</h1>
                </div>

                <div>
                    <Table responsive="sm" bordered hover>
                        <thead>
                            <tr className="text-center fs-5">
                                <th>Order ID Number</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allOrders}
                        </tbody>
                        <tfoot className="col-4"></tfoot>
                    </Table>
                </div>
            </div>
        </>
    );
};