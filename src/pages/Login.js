import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import { Form, Button, Card, Container } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function Login() {

    // Allow us to consume the User Context object and its values(properties) to use for user validation.
    const {user, setUser} = useContext(UserContext);

    // State hooks ...
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);


    useEffect(() => {
        // Validation to enable submit button when all fields are populated
        if (email !== '' && password !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    });

    function authenticate(e) {

        // Prevents page loading / redirection via form submission.
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.accessToken);
            if(data.accessToken !== undefined){
                // The JWT token...
                localStorage.setItem("token", data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Kaizoku PH!"
                });
            } else {
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "Check your login details and try again."
                });
            }
        })

        // Clear input fields
        setEmail('');
        setPassword('');

    };

    
    const retrieveUserDetails = (token) => {

        // Token will be sent as part of the request's header.
        fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Change the global "user" state to store the "id" and "isAdmin" property.
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };


    return (
        // Create a conditional statement that will redirect the user to the course page when a user is already logged in.
        (user.id !== null)
        ?
            <Navigate to="/" />
        :
            <>
                <Container className="w-50 pt-5 mt-5 pb-5 bg-dark">
                <Card>
                    <Card.Body>
                        <h1 className="mb-5">Login</h1>
                        <Form onSubmit={e => authenticate(e)}>
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {
                                isActive
                                ?
                                    <Button
                                        variant="success"
                                        type="submit"
                                        id="submitBtn">
                                        Login
                                    </Button>
                                :
                                    <Button
                                        variant="secondary"
                                        type="submit"
                                        id="submitBtn"
                                        disabled>
                                        Login
                                    </Button>
                            }

                            <p>Don't have an account yet? <a className="register-link" href="/register">Register</a> now!</p>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container> 
            </>
    );
};