import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

// Images
import Logo from "../images/register.png";
import "../App.css"


export default function Register() {

    const navigate = useNavigate();

    // Create state hooks to store the values of the input fields
    const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    
    // Create a state to determine whether the submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if the values are successfully binding
    console.log(fName);
    console.log(lName);
    console.log(email);
    console.log(mobileNo);
    console.log(password1);
    console.log(password2);

    useEffect(() => {

        // Enable the button if:
        // All the fields are populated
        // both passwords match

        if ((fName !== '' &&
            lName !== '' &&
            email !== '' &&
            mobileNo !== '' &&
            password1 !== '' &&
            password2 !== '') &&
            (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [fName, lName, email, mobileNo, password1, password2])

    
    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page loading / redirection via form submission.
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/users/checkEmail`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // if data here is true - meaning email is existing
            if(data) {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    "text": "Kindly provide another email to complete the registration."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/api/users/register`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    // if data here is true - meaning registration is successful or registration is save
                    if(data) {
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Proceed to login."
                        });

                        // Clear input fields
                        setFName('');
                        setLName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        // It will allow us to redirect the user to the login page after account registration
                        navigate("/login");

                    } else {
                        Swal.fire({
                            title: "Something Went Wrong",
                            icon: "error",
                            text: "Please try again."
                        })
                    }
                })
            }
        });
    };

    return (

            <>
                <div className="d-flex">
                    <div className="mx-5 d-flex align-items-center">
                        <img className="img-fluid register-logo" alt="logo" src={Logo} />
                    </div>
                
                    <div className="ms-auto w-100">
                        <h1 className="my-5 text-center">Register Now!</h1>
                        <Form onSubmit={e => registerUser(e)}>
                            <Form.Group className="mb-3" controlId="firstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter first name"
                                    value={fName}
                                    onChange={e => setFName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="lastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter last name"
                                    value={lName}
                                    onChange={e => setLName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="emailAddress">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="mobileNo">
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="09xxxxxxxxx"
                                    value={mobileNo}
                                    onChange={e => setMobileNo(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Enter password"
                                    value={password1}
                                    onChange={e => setPassword1(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password2">
                                <Form.Label>Verify Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Verify password"
                                    value={password2}
                                    onChange={e => setPassword2(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {/* Conditional rendering - submit button will be active based on the isActive state */}
                            {
                                isActive
                                    ?
                                    <Button
                                        variant="success"
                                        type="submit"
                                        id="submitBtn">
                                        Submit
                                    </Button>

                                    :
                                    <Button
                                        variant="secondary"
                                        type="submit"
                                        id="submitBtn"
                                        disabled>
                                        Submit
                                    </Button>
                            }
                        </Form>
                    </div>
                </div>
            </>
    );
};