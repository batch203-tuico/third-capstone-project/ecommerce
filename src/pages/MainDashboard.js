import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";


export default function MainDashboard() {
    return (
        <>
            <div>
                <h1 className="text-center font-bold my-5">Main Dashboard</h1>

                <div className="text-center">
                    <Button variant="warning" as={Link} to="/dashboard/admin/users" className="mx-5" size={"lg"}>
                        <svg style={{marginRight: "8px"}} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                            <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                            <path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z" />
                            <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z" />
                        </svg>
                            All Users
                    </Button>

                    <Button variant="dark" as={Link} to="/dashboard/admin/products" className="mx-5" size={"lg"}>
                        <svg style={{ marginRight: "8px" }} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bag-check-fill" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zm-.646 5.354a.5.5 0 0 0-.708-.708L7.5 10.793 6.354 9.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
                        </svg>
                            All Products
                    </Button>
                </div>
            </div>
        </>
    );
};