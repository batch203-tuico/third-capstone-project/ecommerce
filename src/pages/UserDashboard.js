import { useContext, useEffect, useState } from "react";
import { Navigate, Link } from "react-router-dom";
import { Button, Table} from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function UserDashboard() {

    const { user } = useContext(UserContext);

    const [allUsers, setAllUsers] = useState([]);


    // fetchData() function to get all the admin/notAdmin users.
    const fetchData = () => {
        // get all the users from the database
        fetch(`${process.env.REACT_APP_API_URL}/api/users/`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllUsers(data.map(user => {
                return (
                    <tr className="text-center" key={user._id}>
                        <td>{user._id}</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{user.email}</td>
                        <td>{user.mobileNo}</td>
                        {/* <td>{user.orders}</td> */}
                        <td>{user.isAdmin ? "Admin" : "User"}</td>
                        <td className="d-flex">
                            { // conditional rendering on what button should be visible base on the status of the product
                                (user.isAdmin)
                                ?
                                    <Button
                                        variant="primary"
                                        className="col-10"
                                        size="sm"
                                        onClick={() => setUser(user._id, user.firstName, user.lastName)}>User</Button>
                                :
                                    <>
                                        <Button
                                            variant="danger"
                                            size="sm"
                                            className="mx-1 col-10"
                                            onClick={() => setAdmin(user._id, user.firstName, user.lastName)}>Admin</Button>
                                    </>
                                }
                        </td>
                    </tr>
                )
            }));
        });
    };

    // to fetch all products in the first render of the page.
    useEffect(() => {
        fetchData();
    }, [])



    // [SECTION] Setting the user to Admin/notAdmin

    // Making the user not an admin
    const setUser = (id, firstName, lastName) => {
        console.log(id);
        console.log(firstName);
        console.log(lastName)

        // Using the fetch method to set the isAdmin property of the user to false
        fetch(`${process.env.REACT_APP_API_URL}/api/users/${id}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: false
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data) {
                Swal.fire({
                    title: "Status Successfully Changed!",
                    icon: "success",
                    text: `${firstName} ${lastName} is now a user.`
                })
                // To show the update with the specific operation initiated.
                fetchData();
            } else {
                Swal.fire({
                    title: "Process Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                })
            }
        })
    };


    // Making the user an admin
    const setAdmin = (id, firstName, lastName) => {
        console.log(id);
        console.log(firstName);
        console.log(lastName);

        // Using the fetch method to set the isAdmin property of the user to true
        fetch(`${process.env.REACT_APP_API_URL}/api/users/${id}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: true
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data) {
                Swal.fire({
                    title: "Status Successfully Changed!",
                    icon: "success",
                    text: `${firstName} ${lastName} is now an admin.`
                })
                    // To show the update with the specific operation initiated.
                    fetchData();
            } else {
                Swal.fire({
                    title: "Process Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                })
            }
        })
    };


    return (
        (user.isAdmin)
            ?
            <>
                <h1 className="text-center font-bold my-5">All User Details</h1>
                   
                {/* For viewing all the products in the database. */}
                <Table responsive="sm" striped bordered hover>
                    <thead>
                        <tr className="text-center fs-5">
                            <th>User ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile Number</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allUsers}
                    </tbody>
                </Table>
                <div className="text-center mt-4">
                    <Button as={Link} to="/dashboard" variant="secondary">Back to Dashboard</Button>
                </div>
            </>
            :
                <Navigate to="/products" />
    );
};