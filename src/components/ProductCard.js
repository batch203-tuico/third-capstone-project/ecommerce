import { Link } from "react-router-dom";
import { Card, Button, Container, Row } from "react-bootstrap";

export default function ProductCard({productProp}){

    console.log(productProp);

    // Destructure the product properties into their own variables
    const { _id, name, description, price, stocks } = productProp;

    return (
            <Container fluid>
                <Row className="mr-3 col-12 h-100">
                    <Card className="my-3 card">
                        <Card.Body>
                            <Card.Title className="fs-4" style={{fontWeight: "bolder"}}>{name}</Card.Title>
                            <Card.Subtitle className="fs-4 mt-4">Description:</Card.Subtitle>
                            <Card.Text className="fs-5" style={{fontStyle: "italic"}}>{description}</Card.Text>
                            <Card.Subtitle className="fs-4 mt-3">Price:</Card.Subtitle>
                            <Card.Text className="fs-5" style={{color: "blue"}}>Php {price}.00</Card.Text>
                            <Card.Subtitle className="fs-4 mt-3">Stocks:</Card.Subtitle>
                            <Card.Text className="fs-5">{stocks} available</Card.Text>
                        </Card.Body>

                            <Button className="col-2 mb-3 mx-3" size={"sm"} as={Link} to={`/products/${_id}`} variant="success">Place Order</Button>
                    </Card>
                </Row>    
            </Container> 
            
    );
};