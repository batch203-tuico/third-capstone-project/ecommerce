import { useContext } from "react";
import { NavLink } from "react-router-dom";
import {Container, Nav, Navbar,} from "react-bootstrap";

import UserContext from "../UserContext";

// Navbar logo
import OnePieceLogo from "../images/op-logo.png";


export default function AppNavbar(){

    // Use the user (global state) for the conditional rendering of our app nav bar.
    const {user} = useContext(UserContext);
    
    console.log(user);


    return (
        <Navbar className="navbar-bg" expand="lg">
            <Container fluid className="mx-3">
                <img className="nav-logo" src={OnePieceLogo} />
                <Navbar.Brand className="text-light fs-4 mx-2" as={NavLink} to="/">Kaizoku PH</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">

                    <Nav className="ms-auto">
                    
                        {
                            (user.isAdmin)
                            ?
                                <Nav.Link className="text-light fs-4" as={NavLink} to="/dashboard" end>Dashboard</Nav.Link>
                            :
                                <>
                                    <Nav.Link className="text-light fs-4" as={NavLink} to="/products" end>Shop</Nav.Link>
                                    
                                    
                                </>
                                
                        }

                        {
                            (user.isAdmin === false)
                            ?
                                <Nav.Link className="text-light fs-4" as={NavLink} to="/orders" end>Orders</Nav.Link>
                            :
                                <> </>
                        }
                       
                        {
                            (user.id !== null)
                                ?
                                    <>
                                        <Nav.Link className="text-light fs-4" as={NavLink} to="/logout" end>Logout</Nav.Link>
                                    </>
                                :
                                    <>
                                        <Nav.Link className="text-light fs-4" as={NavLink} to="/login" end>Login</Nav.Link>
                                    </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};