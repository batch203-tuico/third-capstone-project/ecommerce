import { React, useContext } from "react";
import { Container, Carousel } from "react-bootstrap";

import UserContext from "../UserContext";

// Banner Images
import BannerLogo from "../images/banner-logo.jpg";
import BannerLogo2 from "../images/banner-logo2.jpg";
import BannerLogo3 from "../images/banner-logo3.jpg";



export default function Banner(){

    const { user } = useContext(UserContext);

    return (
        <Container fluid>
            <Carousel className="pt-5">
                <Carousel.Item>
                    <img
                        className="d-block w-100 banner-logo img-fluid"
                        src={BannerLogo}
                        alt="First slide"
                    />
                </Carousel.Item>

                <Carousel.Item>
                    <img
                        className="d-block w-100 banner-logo img-fluid"
                        src={BannerLogo3}
                        alt="Second slide"
                    />
                </Carousel.Item>

                <Carousel.Item>
                    <img
                        className="d-block w-100 banner-logo img-fluid"
                        src={BannerLogo2}
                        alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>

            <div className="pt-5 mb-4 text-center">
                <h1 className="font-bold">One Piece</h1>
                <h5 className="mb-3">One Piece is a Japanese manga series written and illustrated by Eiichiro Oda. It has been serialized in Shueisha's shōnen manga magazine Weekly Shōnen Jump since July 1997, with its individual chapters compiled into 103 tankōbon volumes as of August 2022.</h5>

                {
                    (user.isAdmin)
                    ?
                        <button type="button" className="mt-3 btn-home">
                            <a href="/dashboard" className="register-link" style={{color: "black", fontWeight: "bolder"}}>
                                Go to Dashboard
                            </a>
                        </button>
                    :
                        <button type="button" className="mt-3 btn-home">
                            <a href="/products" className="register-link" style={{ color: "black", fontWeight: "bolder" }}>
                            Shop Now!
                            </a>
                        </button>
                }
            </div>
            
        </Container>
    )
};