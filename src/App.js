import { useEffect, useState } from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { Container } from "react-bootstrap"

import { UserProvider } from "./UserContext";
import './App.css';

// All Pages
import AppNavbar from "./components/AppNavbar";
import Home from './pages/Home';
import MainDashboard from "./pages/MainDashboard";
import ProductDashboard from "./pages/ProductDashboard";
import UserDashboard from "./pages/UserDashboard";
import Products from './pages/Products';
import ProductView from "./pages/ProductView";
import Register from "./pages/Register";
import Login from './pages/Login';
import Logout from './pages/Logout';
import Orders from "./pages/Orders";
import Error from "./pages/Error";



function App() {

  // Global state hooks for the user information for validating if a user is logged in.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage
  const unsetUser = () => {

    // Allow us to clear the information in the localStorage
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  // To update the User state upon a page load is initiated and a user already exists.
  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      
      // Set the user states value if the token already exists in the local storage
       if(data._id !== undefined){

         setUser({
           id: data._id,
           isAdmin: data.isAdmin
         });

      // Set back to the initial state of the user if no token found in the local storage
       } else {
         setUser({
           id: null,
           isAdmin: null
          });
        }
      })
  }, [])


  return (
    // We store information in the context by providing the information using the "UserProvider" component and passing the information  via the "value" prop.
      // All the information inside the value prop will be accessible to pages/components wrapped around with the User Provider.
    <UserProvider value={{user, setUser, unsetUser}}>
    {/* Router component is used to wrapped around all components which will have access to the routing system. */}
      <Router>
        <AppNavbar />
        <Container>
        {/* Routes holds all our Route components. */}
          <Routes>
          {/* 
              Route assign and endpoint and display the appropriate page component for that endpoint.
                - exact and path props to assign the endpoint and the page should be only accessed on th specific endpoint
                - "element" props assigns page components to the displayed endpoint
          */}
            <Route exact path="/" element={<Home />} />
            <Route exact path="/dashboard" element={<MainDashboard />} />
            <Route exact path="/dashboard/admin/products" element={<ProductDashboard />} />
            <Route exact path="/dashboard/admin/users" element={<UserDashboard />} />
            <Route exact path="/products" element={<Products />} />
            <Route exact path="/products/:productId" element={<ProductView />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/orders" element={<Orders />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/*" element={<Error />} />
            
          </Routes>

        </Container>
        
      </Router>
    </UserProvider>
  );
};

export default App;
